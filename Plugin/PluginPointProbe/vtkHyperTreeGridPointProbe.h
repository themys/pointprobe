/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkHyperTreeGridPointProbe.h

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/**
 * @class   vtkHyperTreeGridPointProbe
 * @brief   Extract fragments from HyperTreeGrid
 *
 *
 * ...
 *
 * @sa
 * vtkHyperTreeGrid vtkHyperTreeGridAlgorithm
 *
 * @par Thanks:
 * This work was supported by Commissariat a l'Energie Atomique
 * CEA, DAM, DIF, F-91297 Arpajon, France.
 */

#ifndef VTK_HYPER_TREE_GRID_POINTPROBE_H
#define VTK_HYPER_TREE_GRID_POINTPROBE_H

#include <list>
#include <map>
#include <set>
#include <vector>

#include "vtkPolyDataAlgorithm.h"
#include "vtkHyperTreeGridOrientedGeometryCursor.h"
//--------------------------------------------------------------------------------------------------------------------

class vtkHyperTreeGridPointProbe : public vtkPolyDataAlgorithm
{
public:
  vtkTypeMacro(vtkHyperTreeGridPointProbe, vtkPolyDataAlgorithm);
  virtual void PrintSelf(ostream& os, vtkIndent indent) override;

  static vtkHyperTreeGridPointProbe* New();

protected:
  vtkHyperTreeGridPointProbe() = default;
  virtual ~vtkHyperTreeGridPointProbe() override = default;

  int RequestData(vtkInformation*, vtkInformationVector**, vtkInformationVector*) override;
  int FillInputPortInformation(int port, vtkInformation* info) override;

private:
  vtkHyperTreeGridPointProbe(const vtkHyperTreeGridPointProbe&) = delete;
  void operator=(const vtkHyperTreeGridPointProbe&) = delete;
};

#endif // VTK_HYPER_TREE_GRID_POINTPROBE_H
