/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkHyperTreeGridPointProbe.cxx

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "vtkHyperTreeGridPointProbe.h"

#include "vtkBitArray.h"
#include "vtkCellData.h"
#include "vtkDataObject.h"
#include "vtkDoubleArray.h"
#include "vtkHyperTreeGrid.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkMultiBlockDataSet.h"
#include "vtkObjectFactory.h"
#include "vtkPointData.h"
#include "vtkPolyData.h"
#include "vtkSmartPointer.h"
#include "vtkStreamingDemandDrivenPipeline.h"

#include "vtkMPIController.h"
#include "vtkMultiProcessController.h"
//-------------------------------------------------------------------------------------------------
vtkStandardNewMacro(vtkHyperTreeGridPointProbe);
//----------------------------------------------------------------------------
int vtkHyperTreeGridPointProbe::RequestData(vtkInformation* vtkNotUsed(request),
  vtkInformationVector** inputVector,
  vtkInformationVector* outputVector)
{
  // Get the information objects
  vtkInformation* inInfo1 = inputVector[0]->GetInformationObject(0);
  // vtkInformation* inInfo2 = inputVector[1]->GetInformationObject(0);
  vtkInformation* outInfo = outputVector->GetInformationObject(0);

  // Retrieve input and output
  vtkHyperTreeGrid* input1 =
    vtkHyperTreeGrid::SafeDownCast(inInfo1->Get(vtkDataObject::DATA_OBJECT()));
  std::cerr << "input1:" << input1 << std::endl;
  if (!input1)
  {
    return 0;
  }
  // vtkPolyData* input2 = vtkPolyData::SafeDownCast(inInfo2->Get(vtkDataObject::DATA_OBJECT()));
  // std::cerr << "input2:" << input << std::endl;
  vtkPolyData* output = vtkPolyData::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));

  std::cerr << "output:" << output << std::endl;

  vtkCellData* inCD = input1->GetCellData();

  vtkIntArray* GhostArray =
    vtkIntArray::SafeDownCast(inCD->GetArray(vtkDataSetAttributes::GhostArrayName()));

  vtkPointData* outPD = output->GetPointData();

  vtkIdType numCells = input1->GetNumberOfLeaves();

  if (numCells == 0)
  {
    vtkDebugMacro(<< "No cells in mesh to generate points for");
    return 1;
  }

  vtkNew<vtkPoints> newPts;
  newPts->SetDataTypeToDouble();
  newPts->SetNumberOfPoints(numCells);
  vtkDoubleArray* pointArray = vtkDoubleArray::SafeDownCast(newPts->GetData());

  vtkNew<vtkIdList> pointIdList;
  pointIdList->SetNumberOfIds(numCells);

  vtkNew<vtkIdList> cellIdList;
  cellIdList->SetNumberOfIds(numCells);

  /*
    // Remove points that would have been produced by empty cells
    // This should be multithreaded someday
    bool hasEmptyCells = false;
    vtkTypeBool abort = 0;
    vtkIdType progressInterval = numCells / 10 + 1;
    vtkIdType numPoints = 0;
    for (vtkIdType cellId = 0; cellId < numCells && !abort; ++cellId)
    {
      if (!(cellId % progressInterval))
      {
        vtkDebugMacro(<< "Processing #" << cellId);
        this->UpdateProgress((0.5 * cellId / numCells) + 0.5);
        abort = this->GetAbortExecute();
      }

      if (input1->GetCellType(cellId) != VTK_EMPTY_CELL)
      {
        newPts->SetPoint(numPoints, newPts->GetPoint(cellId));
        pointIdList->SetId(numPoints, numPoints);
        cellIdList->SetId(numPoints, cellId);
        numPoints++;
      }
      else
      {
        hasEmptyCells = true;
      }
    }

    if (abort)
    {
      return 0;
    }

    newPts->Resize(numPoints);
    pointIdList->Resize(numPoints);
    cellIdList->Resize(numPoints);
    output->SetPoints(newPts);

    if (this->CopyArrays)
    {
      if (hasEmptyCells)
      {
        outPD->CopyAllocate(inCD, numPoints);
        outPD->CopyData(inCD, cellIdList, pointIdList);
      }
      else
      {
        outPD->PassData(inCD); // because number of points == number of cells
      }
    }

    if (this->VertexCells)
    {
      vtkNew<vtkIdTypeArray> iArray;
      iArray->SetNumberOfComponents(1);
      iArray->SetNumberOfTuples(numPoints * 2);
      for (vtkIdType i = 0; i < numPoints; i++)
      {
        iArray->SetValue(2 * i, 1);
        iArray->SetValue(2 * i + 1, i);
      }

      vtkNew<vtkCellArray> verts;
      verts->AllocateEstimate(numPoints, 1);
      verts->ImportLegacyFormat(iArray);
      output->SetVerts(verts);
      outCD->ShallowCopy(outPD);
    }
  */

  output->Squeeze();
  this->UpdateProgress(1.0);
  return 1;
}
//----------------------------------------------------------------------------
/*int vtkHyperTreeGridPointProbe::FillInputPortInformation(int, vtkInformation* info)
{
  info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkMultiBlockDataSet");
  return 1;
}*/
int vtkHyperTreeGridPointProbe::FillInputPortInformation(int vtkNotUsed(port), vtkInformation* info)
{
  info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkHyperTreeGrid");
  return 1;
}
//----------------------------------------------------------------------------
void vtkHyperTreeGridPointProbe::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}
//--------------------------------------------------------------------------------------------------
