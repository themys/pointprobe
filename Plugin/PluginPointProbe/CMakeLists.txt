#==============================================================================
set(classes vtkHyperTreeGridPointProbe)
#==============================================================================
string(TIMESTAMP CMAKE_POINTPROBE_DATE "%d/%m/%Y %H:%M:%S")
#target_compile_options(Love PRIVATE -DLOVE_DATE=\"${CMAKE_LOVE_DATE}\")
#==============================================================================
vtk_module_add_module(PluginPointProbe CLASSES ${classes})
#==============================================================================
paraview_add_server_manager_xmls(XMLS PointProbe.xml)
#==============================================================================
